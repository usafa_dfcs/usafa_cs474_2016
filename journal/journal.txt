This is a place holder for your journal. 

You can use any type of file you want to keep your journal. (Word document, Excel document, etc.)

Git only does version control on files (not folders). You can delete this file after you create
your journal file.